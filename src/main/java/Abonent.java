import java.util.HashSet;
import java.util.Set;

public final class Abonent {
    private final Number id;
    private final Set<Abonent> friends = new HashSet();

    public Abonent (Number id){
        this.id = id;
    }
    public void addFriend(Abonent friend){
        friends.add(friend);
    }
    public Number getId(){
        return id;
    }
    public Set<Abonent> getFriends() {
        return friends;
    }

    public static Set<Abonent> getWeb(int n, Set<Abonent> last){
        return getWeb(n, last, new HashSet());
    }
    private static Set<Abonent> getWeb(int n, Set<Abonent> last, Set<Abonent> all){
        if(n == 1) {
            all.addAll(last);
            return all;
        } else {
            Set<Abonent> newLast = new HashSet();
            for(Abonent a : last)
                newLast.addAll(a.friends);
            all.addAll(last);
            return getWeb(n-1, newLast, all);
        }
    }
}

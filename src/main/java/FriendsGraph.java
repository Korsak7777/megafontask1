import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class FriendsGraph {
    public static Map<Number, Set<Number>> friendsGraph(Collection<Number[]> s, int n){
        Map<Number, Abonent> res = new HashMap();

        for(Number[] pair : s) {
            Abonent caller = res.getOrDefault(pair[0], new Abonent(pair[0]));
            Abonent answering = res.getOrDefault(pair[1], new Abonent(pair[1]));

            caller.addFriend(answering);

            if(!res.containsKey(pair[0]))
                res.put(pair[0], caller);
            if(!res.containsKey(pair[1]))
                res.put(pair[1], answering);
        }

        return res.values().stream()
                .collect(Collectors.toMap(Abonent::getId,
                        it -> Abonent.getWeb(n, it.getFriends()).stream()
                                .map(Abonent::getId)
                                .collect(Collectors.toSet())
                        )
                );
    }

    public static List<Number[]> generateAbonents(int blockSize){
        int resultSize = blockSize * 5;
        long origin = 9000000000L;
        long bound = 9999999999L;

        Long[] x = new Random().longs(blockSize, origin, bound)
                .boxed().collect(Collectors.toSet()).toArray(new Long[blockSize]);

        ArrayList<Number[]> res = new ArrayList(resultSize);
        int i = 0;
        Random r = new Random();
        while (i++<resultSize)
            res.add(new Long[]{x[r.nextInt(blockSize)], x[r.nextInt(blockSize)]});

        return res;
    }

    public static void main(String[] args) {
        Set<Number[]> s = new HashSet<>();
        s.add(new Number[]{1,2});
        s.add(new Number[]{1,3});
        s.add(new Number[]{2,5});
        s.add(new Number[]{2,6});
        s.add(new Number[]{3,7});
        s.add(new Number[]{5,2});
        s.add(new Number[]{5,7});
        s.add(new Number[]{7,1});
        s.add(new Number[]{7,6});

        System.out.println(friendsGraph(generateAbonents(1000000), 1));
        System.out.println(friendsGraph(s, 2));
        System.out.println(friendsGraph(s, 3));
    }
}
